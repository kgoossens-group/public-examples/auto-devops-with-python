# python-flask-autodevops

AutoDevOps template for Python Flask web app

Set `TEST_DISABLED` to `True` as a CICD variable. Without this the Auto DevOps will fail at test stage.