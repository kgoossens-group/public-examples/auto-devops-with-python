FROM python:3
WORKDIR /usr/src/app
RUN pip install Flask
COPY . /usr/src/app
ENV PORT 5000
EXPOSE $PORT
CMD [ "flask", "run", "--host=0.0.0.0" ]
